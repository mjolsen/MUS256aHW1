// Music 256a / CS 476a | fall 2016
// CCRMA, Stanford University
//
// Author: Romain Michon (rmichonATccrmaDOTstanfordDOTedu)
// Description: Simple JUCE sine wave synthesizer

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Sine.h"

class MainContentComponent :
    public AudioAppComponent,
    private Slider::Listener,
    private ToggleButton::Listener
{
public:
    MainContentComponent() : gain(0.0), onOffMain(0), samplingRate(0.0), playValue(0.0f), 
                             nOscCur(1), nOscPrev(1), targetFreq(220.0), oscGainInc(0.0),
                             atkInc(0.0), relInc(0.0)
    {
        // configuring frequency slider and adding it to the main window
        addAndMakeVisible (frequencySlider);
        frequencySlider.setRange (50.0, 5000.0);
        frequencySlider.setSkewFactorFromMidPoint (500.0);
        frequencySlider.setValue(targetFreq); // will also set the default frequency of the sine osc
        frequencySlider.addListener (this);
        
        // configuring frequency label box and adding it to the main window
        addAndMakeVisible(frequencyLabel);
        frequencyLabel.setText ("Frequency", dontSendNotification);
        frequencyLabel.attachToComponent (&frequencySlider, true);
        
        // configuring gain slider and adding it to the main window
        addAndMakeVisible (gainSlider);
        gainSlider.setRange (0.0, 1.0);
        gainSlider.setValue(0.5); // will alsi set the default gain of the sine osc
        gainSlider.addListener (this);
        
        // configuring gain label and adding it to the main window
        addAndMakeVisible(gainLabel);
        gainLabel.setText ("Gain", dontSendNotification);
        gainLabel.attachToComponent (&gainSlider, true);

        // configuring play button and adding it to the main window
        addAndMakeVisible(playButton);
        playButton.setButtonText(TRANS("Click or press space bar to play"));
        playButton.addListener(this);
                
        // configuring play label and adding it to the main window
        addAndMakeVisible(playLabel);
        playLabel.setText ("Play", dontSendNotification);
        playLabel.attachToComponent (&playButton, true);
    
        // configuring # osc knob and adding it to the main window
        addAndMakeVisible (nOscKnob);
        nOscKnob.setRange (1, 5, 1);
        nOscKnob.setSliderStyle (Slider::Rotary);
        nOscKnob.setTextBoxStyle (Slider::TextBoxBelow, false, 80, 20);
        nOscKnob.addListener (this);
        
        // configuring # osc label box and adding it to the main window
        addAndMakeVisible(nOscLabel);
        nOscLabel.setText ("# Osc", dontSendNotification);
        nOscLabel.setJustificationType (Justification::centred);
        nOscLabel.attachToComponent (&nOscKnob, false);
        
        // configuring attack knob and adding it to the main window
        addAndMakeVisible (atkKnob);
        atkKnob.setRange (20, 2000, 20);
        atkKnob.setSliderStyle (Slider::Rotary);
        atkKnob.setTextBoxStyle (Slider::TextBoxBelow, false, 80, 20);
        atkKnob.addListener (this);
        
        // configuring attack label box and adding it to the main window
        addAndMakeVisible(atkLabel);
        atkLabel.setText ("Attack (ms)", dontSendNotification);
        atkLabel.setJustificationType (Justification::centred);
        atkLabel.attachToComponent (&atkKnob, false);
        atkKnob.setValue(200);
        
        // configuring release knob and adding it to the main window
        addAndMakeVisible (relKnob);
        relKnob.setRange (200, 5000, 50);
        relKnob.setSliderStyle (Slider::Rotary);
        relKnob.setTextBoxStyle (Slider::TextBoxBelow, false, 80, 20);
        relKnob.addListener (this);
        
        // configuring release label box and adding it to the main window
        addAndMakeVisible(relLabel);
        relLabel.setText ("Release (ms)", dontSendNotification);
        relLabel.setJustificationType (Justification::centred);
        relLabel.attachToComponent (&relKnob, false);
        relKnob.setValue(1000);
        
        // configuring base knob and adding it to the main window
        addAndMakeVisible (baseKnob);
        baseKnob.setRange (1.0, 3.9999, 0.0001);
        baseKnob.setSliderStyle (Slider::Rotary);
        baseKnob.setTextBoxStyle (Slider::TextBoxBelow, false, 80, 20);
        baseKnob.addListener (this);
        
        // configuring base label box and adding it to the main window
        addAndMakeVisible(baseLabel);
        baseLabel.setText ("Pow base", dontSendNotification);
        baseLabel.setJustificationType (Justification::centred);
        baseLabel.attachToComponent (&baseKnob, false);
        baseKnob.setValue(2.0);
        
        // configuring exponent knob and adding it to the main window
        addAndMakeVisible (expKnob);
        expKnob.setRange (0.0, 1.0, 0.000001);
        expKnob.setSliderStyle (Slider::Rotary);
        expKnob.setTextBoxStyle (Slider::TextBoxBelow, false, 80, 20);
        expKnob.addListener (this);
        
        // configuring exponent label box and adding it to the main window
        addAndMakeVisible(expLabel);
        expLabel.setText ("Pow exp", dontSendNotification);
        expLabel.setJustificationType (Justification::centred);
        expLabel.attachToComponent (&expKnob, false);
        float tempExp = 3.0/12.0;
        expKnob.setValue(tempExp);
        
        setSize (600, 250);
        nChans = 1;
        setAudioChannels (0, nChans); // no inputs, one output
    }
    
    ~MainContentComponent()
    {
        shutdownAudio();
    }
    
    void resized() override
    {
        // placing the UI elements in the main window
        // getWidth has to be used in case the window is resized by the user
        const int sliderLeft = 80;
        const int knobWidth = (int)(((float)getWidth() - float(sliderLeft) - 20.0)/5.0);
        frequencySlider.setBounds (sliderLeft, 10, getWidth() - sliderLeft - 20, 20);
        gainSlider.setBounds (sliderLeft, 40, getWidth() - sliderLeft - 20, 20);
        playButton.setBounds (sliderLeft, 70, getWidth() - sliderLeft - 20, 20);
        nOscKnob.setBounds (sliderLeft, 120, knobWidth, 72);
        atkKnob.setBounds (sliderLeft+knobWidth, 120, knobWidth, 72);
        relKnob.setBounds (sliderLeft+2*knobWidth, 120, knobWidth, 72);
        baseKnob.setBounds (sliderLeft+3*knobWidth, 120, knobWidth, 72);
        expKnob.setBounds (sliderLeft+4*knobWidth, 120, knobWidth, 72);
    }
    
    void sliderValueChanged (Slider* slider) override
    {
        if (samplingRate > 0.0){
            // if frequency slider changed
            if (slider == &frequencySlider){
                // update targetFreq
                targetFreq = frequencySlider.getValue();
            }
            // if gain slider
            else if (slider == &gainSlider){
                // set gain value
                gain = gainSlider.getValue();
            }
            // if attack knob
            else if (slider == &atkKnob){
                // set attack increment value, convert ms to samp
                atkInc = 1.0/((atkKnob.getValue()/1000.0)*samplingRate);
            }
            // if release knob
            else if (slider == &relKnob){
                // set release increment value, convert ms to samp
                relInc = 1.0/((relKnob.getValue()/1000.0)*samplingRate);
            }
            // if base knob, get new value
            else if (slider == &baseKnob){
                base = baseKnob.getValue();
            }
            // if exponent knob, get new value
            else if (slider == &expKnob){
                exp = expKnob.getValue();
            }
            // if number of oscs slider
            else if (slider == &nOscKnob){
                // store new number of oscs
                nOscCur = nOscKnob.getValue();
                //String message;
                //message << "new osc count: " << nOscCur << ". Prev osc count: " << nOscPrev << "\n";
                        
                // if osc count increased
                if (nOscCur > nOscPrev){
                    // calc number of new oscs
                    int nOscToAdd = nOscCur - nOscPrev;
                    // add them using add function
                    for (int i = 0; i < nOscToAdd; i++){
                        addOscillator(&sines,&onOffs,&playValues,baseFilt[0],expFilt[0]);
                    }
                }
                // if oscillators decreased, marked ones to be inactivated
                else if (nOscCur < nOscPrev){
                    int nOscToRemove = nOscPrev - nOscCur;
                    
                    // set correct number of end oscs to remove to off
                    std::fill(onOffs.end()-nOscToRemove,onOffs.end(),0);
                }
                //message << "there are currently " << nOscPrev << " oscillators.\n";
                //Logger::getCurrentLogger()->writeToLog (message);
            }
        }
    }
    
    // function to return highest frequency of osc with on/off value of 1 (meaning active)
    float findTopFreq(std::vector<Sine *>* sinesLoc, std::vector<int>* onOffsLoc){
        // determine number of osc
        int numSines = (*sinesLoc).size();
        float topFreq = (*sinesLoc).front()->getFrequency();
        
        if (numSines > 1){
            for (int i = 1; i < numSines; i++){
                if ((*onOffsLoc)[i-1] == 1){
                    float tempFreq = (*sinesLoc)[i]->getFrequency();
                    topFreq = std::max(tempFreq,topFreq);
                }
            }
        }
        return topFreq;
    }
    
    // function to add new oscillator to sines vector, value 1 (active) to onOffs and value 0.0
    // to playValues (DSP loop will handle increasing value to 1.0)
    void addOscillator(std::vector<Sine *>* sinesLoc, std::vector<int>* onOffsLoc, 
                       std::vector<float>* playValuesLoc, float baseLoc, float expLoc){
        // base new freq off of highest freq of osc with onOff = 1
        float oscFreq = findTopFreq(&sines,&onOffs) * std::pow(baseLoc,expLoc);
        
        //String message;
        //message << "Adding new osc with freq: " << oscFreq << "\n";

        // add new osc to end of vector then set it's SR and freq
        (*sinesLoc).push_back(new Sine());
        (*sinesLoc).back()->setSamplingRate(samplingRate);
        (*sinesLoc).back()->setFrequency(oscFreq);
        // add onOff value of 1 and playValue of 0.0
        (*onOffsLoc).push_back(1);
        (*playValuesLoc).push_back(0.0);
        nOscPrev += 1;
        //message << "nOscCur: " << nOscCur << " nOscPrev: " << nOscPrev << "\n";        
        //Logger::getCurrentLogger()->writeToLog (message);
    }
    
    // function to remove osc and related on/off and play values for specified osc index
    void deleteOscillator(std::vector<Sine *>* sinesLoc, std::vector<int>* onOffsLoc, 
                          std::vector<float>* playValuesLoc, int locIndex){
        //String message;
        std::vector<Sine*>::iterator it = (*sinesLoc).begin() + locIndex;
        delete (*it);
        (*sinesLoc).erase(it);
        (*onOffsLoc).erase((*onOffsLoc).begin()+locIndex-1);
        (*playValuesLoc).erase((*playValuesLoc).begin()+locIndex-1);
        nOscPrev -= 1;
        //message << "nOscCur: " << nOscCur << " nOscPrev: " << nOscPrev << "\n";
        //Logger::getCurrentLogger()->writeToLog (message);
    }

    // function to delete oscs whose onOff=0 and playValue=0.0
    void removeInactiveOscs(std::vector<Sine *>* sinesLoc, std::vector<int>* onOffsLoc, 
                            std::vector<float>* playValuesLoc){
        // if there is more than 1 osc
        if ((*sinesLoc).size() > 1){
            // cycle through them backwards
            for (int i = sines.size()-1; i > 0; i--){
                // remove any inactive ones
                if ((*onOffsLoc)[i-1] == 0 && (*playValuesLoc)[i-1] == 0.0)
                    deleteOscillator(&(*sinesLoc),&(*onOffsLoc),&(*playValuesLoc),i);
            }
        }
    }

    // function to update playValues that need updating
    void updatePlayValues(std::vector<int>* onOffsLoc, std::vector<float>* playValuesLoc, float playIncLoc){
        // cycle through relevant playValues
        for (int i = 0; i < (*onOffsLoc).size(); i++){
            // increment relevant playValues
            if ((*onOffsLoc)[i] == 1 && (*playValuesLoc)[i] < 1.0){
                double tempVal = (*playValuesLoc)[i] + (playIncLoc);
                (*playValuesLoc)[i] = std::min(1.0,tempVal);
            }
            // decrement relevant playValues
            else if ((*onOffsLoc)[i] == 0 && (*playValuesLoc)[i] > 0.0){
                double tempVal = (*playValuesLoc)[i] - (playIncLoc);
                (*playValuesLoc)[i] = std::max(0.0,tempVal);
            }
        }
    }

    void updateFrequencies(std::vector<Sine *>* sinesLoc, std::vector<int>* onOffsLoc, float baseFreq, 
                           float baseLoc, float expLoc){
        // set frequency of 1st osc to base frequency
        sines[0]->setFrequency(baseFreq);
        // if more than 1 osc
        if ((*sinesLoc).size() > 1){
            // loop through rest and set freq based on freq of prev osc for currently active oscs
            for (int i = 1; i < (*sinesLoc).size(); i++){
                if ((*onOffsLoc)[i-1] == 1)
                        (*sinesLoc)[i]->setFrequency((*sinesLoc)[i-1]->getFrequency() * std::pow(baseLoc,expLoc));
            }
        }
    }
    
    void buttonClicked (Button* button) override
    {   
        // detects release event of textButton and then turns off audio
        // if it is currently set to on
        if(button == &playButton && onOffMain == 1)
            onOffMain = 0;
    }
    
    void buttonStateChanged (Button* button) override
    {
        // detect buttonDown state and turns on audio if it isn't already on
        if(button == &playButton && onOffMain == 0 && button->getState() == 2)
            onOffMain = 1;
    }

    void prepareToPlay (int /*samplesPerBlockExpected*/, double sampleRate) override
    {
        samplingRate = sampleRate; // set the sampling rate
        // push first sine onto sines vector and set it's SR
        for (int i = 0; i < nOscPrev; i++){
            sines.push_back(new Sine());
            sines[i]->setSamplingRate(sampleRate);
        }
        // initialize attack and relase increments
        atkInc = 1.0/((atkKnob.getValue()/1000.0)*samplingRate);
        relInc = 1.0/((relKnob.getValue()/1000.0)*samplingRate);
        
        // initialize frequency filter states
        for (int i = 0; i < 2; i++)
            freqFilt[i] = atkFilt[i] = relFilt[i] = baseFilt[i] = expFilt[i] = gfFilt[i] = 0;
    }

    void releaseResources() override{
        // pop all Sine objects and release their memory
        for (std::vector<Sine*>::iterator it = sines.begin(); it != sines.end(); ++it)
            delete (*it);
        sines.clear();
        onOffs.clear();
        playValues.clear();
    }
    
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override
    {
        // getting the audio output buffer to be filled
        float* const buffer = bufferToFill.buffer->getWritePointer (0, bufferToFill.startSample);
        
        // set oscRemoveInc to a value that will cause removed oscillators to drop to zero
        // and be deallocated during the current audio block
        if (oscGainInc == 0.0){
            oscGainInc = 1.0/((double)(bufferToFill.numSamples - 5));
        }
        
        // set gainFactor based on number of oscs
        float gainFactor = 1.0/(double)nOscCur;
        
        // computing one block
        for (int sample = 0; sample < bufferToFill.numSamples; ++sample)
        {
            // process filters
            freqFilt[0] = (0.999)*freqFilt[1] + (0.001)*targetFreq;
            atkFilt[0] = (0.999)*atkFilt[1] + (0.001)*atkInc;
            relFilt[0] = (0.999)*relFilt[1] + (0.001)*relInc;
            baseFilt[0] = (0.999)*baseFilt[1] + (0.001)*base;
            expFilt[0] = (0.999)*expFilt[1] + (0.001)*exp;
            gfFilt[0] = (0.999)*gfFilt[1] + (0.001)*gainFactor;
            
            // update osc freqs with filtered frequency
            updateFrequencies(&sines,&onOffs,freqFilt[0],baseFilt[0],expFilt[0]);
            //store filter states
            freqFilt[1] = freqFilt[0];
            atkFilt[1] = atkFilt[0];
            relFilt[1] = relFilt[0];
            baseFilt[1] = baseFilt[0];
            expFilt[1] = expFilt[0];
            gfFilt[1] = gfFilt[0];
            
            // variable to compute output value with
            // get value of 1st oscillator which always exists
            float outputVal = sines[0]->tick();
            
            // if there is more than 1 oscillator, add the values of the rest
            // multiplied by their corresponding play values to outputVal
            if (sines.size() > 1){
                // add value of each oscillator to outputVal
                for (int i = 1; i < sines.size(); i++){
                    float playValTemp = playValues[i-1];
                    outputVal += (sines[i]->tick() * playValTemp);
                }
                
                // handle playValue bookkeeping
                updatePlayValues(&onOffs, &playValues,oscGainInc);
            }
            
            // multiple the output value by gainFactor and gain values
            outputVal = outputVal * gain * gfFilt[0];
            
            // handling attack/release behavior if necessary
            if (onOffMain == 1){
                outputVal *= playValue;
                buffer[sample] = outputVal;
                
                if (playValue < 1.0)
                    playValue = std::min(1.0f,playValue+atkInc);
            }
            else if (onOffMain == 0 and playValue > 0.0){
                outputVal *= playValue;
                buffer[sample] = outputVal;
                
                if (playValue > 0.0)
                    playValue = std::max(0.0f,playValue-relInc);
            }
            else
                buffer[sample] = 0.0;
        }
        
        // osc clean up: remove inactive oscillators
        removeInactiveOscs(&sines,&onOffs,&playValues);
    }

private:
    // UI Elements
    Slider frequencySlider, gainSlider, nOscKnob, atkKnob, relKnob, baseKnob, expKnob;
    TextButton playButton;
    
    Label frequencyLabel, gainLabel, playLabel, nOscLabel, atkLabel, relLabel, baseLabel, expLabel;
    
    // vector of Sine objects
    std::vector<Sine *> sines;
    // individual on/offs to use as oscillators added/removed
    std::vector<int> onOffs;
    std::vector<float> playValues;
    
    // Global Variables
    float gain, playValue, base, exp, oscGainInc, targetFreq, freqFilt[2], atkFilt[2], relFilt[2], 
          baseFilt[2], expFilt[2], gfFilt[2], atkInc, relInc;
    int onOffMain, samplingRate, nChans, nOscCur, nOscPrev;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};

Component* createMainContentComponent()     { return new MainContentComponent(); }

#endif  // MAINCOMPONENT_H_INCLUDED