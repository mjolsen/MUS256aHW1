# BasicAudio - MUS256a, HW #1

This application is a standalone audio application that implements additive synthesis. The user can choose between 1 and 5 sinusoid oscillators where each oscillators frequency is determined by the frequency of the previous oscillator multiplied by a ratio of the form base^exponent where base and exponent can be controlled by the user. The frequency of the first oscillator is also set by the user. The attack and release times are set by the user and control the onset and decay of the sound as determined by when the play button is depressed and then released. Additionally, there is a gain control. Internally, the overall gain is also scaled by the value 1/(# of osc) to keep the gain from increasing as the number of oscillators are increased. All parameters are ran through smoothing filters in the dsp loop to prevent glitching.

---

Implemented by Michael Olsen (mjolsenATccrmaDOTstanfordDOTedu) based on starter code by Romain Michon (rmichonATccrmaDOTstanfordDOTedu) for Music 256a / CS 476a (fall 2016).
